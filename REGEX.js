//REGEX

let re = /hello/;

re = /hello/i;
// ako stavimo i onda je case insensitive

re = /hello/g;
// ako stavimo g onda se ne zaustavlja na prvoj reci koju nadje

re = /^h/i;
// ^ znaci da mora da pocinje sa tim slovom ili recju , u ovom slucaju h

re = /world$/i;
// $ znaci da mora da se zavrsava sa tim slovom ili recju , u ovom slucaju world

re = /^hello$/i;
// mora i da pocinje i da se zavrsava sa zadatom recju ili slovom , u ovom slucaju hello

re = /h.llo/i;
// kada stavimo . onda proverava da li na tom mestu stoji bilo koji karakter , ali samo jedan karakter

re = /h*llo/i;
// kada stavimo * onda proverava da li na tom mestu stoji bilo koji karakter , ali moze da bude bilo koji br karaktera ili da ga nema uopste

re = /gre?a?y/i;
// kada posle karaktera stavimo ? to znaci da je to opcioni karakter tako da ce biti true biti gray,grey ili gry , ali griy  ce biti false

re = /gr[ae]y/i;
// ovo znaci da mora da bude a ili e , ne moze ae

re = /[GF]ray/;
// znaci da mora da pocinje sa G ili F

re = /[^GF]ray/;
// znaci da ne sme da pocinje sa G ili F

re = /[A-Z]ray/;
//znaci da mora da pocinje sa bilo kojim velikim slovom

re = /[a-z]ray/;
//znaci da mora da pocinje sa bilo kojim malim slovom

re = /[0-9]ray/
// znaci da mora da pocinje sa bilo kojim brojem

re = /hel{2}o/i;
// znaci da mora da ima 2 slova l

re = /hel{2,5}o/i;
// znaci da mora da ima izmedju 2 i 5 slova l

re = /hel{2,}o/i;
// znaci da mora da ima minimum 2 slova l

re = /([0-9]x){3}/;
// znaci da mora da ima broj + slovo x i tako 3 puta , npr 3x3x3x

re = /\w/;
// proverava da li postoji karakter , bilo da je broj , slovo ili _

re = /\w+/;
//ne zaustavlja se na prvom karakteru nego nastavlja dalje

re = /\W/;
//proverava da li postoji karakter koji nije broj , slovo ili _

re = /\d/;
// proverava da li postoji broj , true ako naidje na broj

re = /\d+/;
// proverava da li postoji broj i ne zaustavlja se kod prvog nadjenog

re = /\D/;
// proverava da li postoji karakter da nije broj , true ako nema brojeva

re = /\s/;
// proverava da li postoji razmak(space) , true ako postoji

re = /\S/;
// proverava da li postoji razmak(space) , true ako ne postoji

re = /hell\b/i;
str = 'hello , welcome to hell';
// b se koristi kada se trazi cela rec , tj ovde nece prijaviti hello kako bi inace prijavio nego tek kada dodje do hell

re = /x(?=y)/;
// true ako posle x bilo gde ide y

re = /x(?!y)/;
// true ako posle x nije y




//vecina provera je radjena ovako
re.test(str);



//* exec()
const result = re.exec('hello world');
// result je u obliku niza koji sadrzi izmedju ostalog i 'index' gde pocinje rec , rec koja se trazi i celu frazu
// ako ne nadje vraca null

//* test()
const result = re.test('Hello');
// result je true ili false

//* replace()
re = /hello/i;
const str = 'hello world';
const newStr = str.replace(re , 'hi');
// pronalazi rec koju zadajemo i menja je sa recju koju saljemo kao drugi parametar = 'hi world'