//DOM

// setovanje textualnog sadrzaja
document.querySelector('#someid').textContent = 'some text content';

// setovanje celog html sadrzavaja
document.querySelector('#someid').innerHTML = `
    <div class="div-class">
        <p class="some-class">Paragraph content</p>
    </div>
`;

