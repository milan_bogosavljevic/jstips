
// da bi se koristilo vise html elemenata jedan za drugim moraju da budu u sklopu div wrapper elementa

// unutar html tagova javascript code se ubacuje tako sto se otvore zagrade {} i unutra se pise js

// kada koristimo and operator prikazivace paragraf ako prodje prethodni uslov

import React from "../learn-react4/React-For-Beginners-Starter-Files/catch-of-the-day/node_modules/react";

{(user.age && user.age > 17) && <p>Age: {user.age}</p>}

//ternarni moze da se koristi regularno

<h1>{user.name ? user.name : "Anonymous"}</h1> 

// imena klasa moraju da imaju prvo slovo veliko
// kada saljemo parametre kroz tag klase key je rezervisana rec <Option key={option} optionTxt={option} /> a ostalim pristupamo this.props.optionTxt
// componente mogu da se renderuju samo u smeru parent->child
// ako nema potrebe za state-ovima brza varijanta je da komponenta bude funkcija , a ne klasa
// componentDidMount moze da se stavlja samo u class komponente
// poenta sa export default je sto moze da se koristi u klasi u koju se importuje pod kojim god imenom (bzvz)

// webpack moze da sadrzi sourcemap configuraciju koja nam pomaze da u devtools vidimo tacno u kojoj liniji je bug (devtool: "cheap-module-eval-source-map")

// kada se radi setState vraca se objekat samo sa tim propertijem koji se menja , ne mora da se dupliraju svi propertiji

// ako nas ne interesuje prethodni state moze da se radi samo setState i da se prosledi objekat , ako nas interesuje mora da se prosledi funkcija koja ima parametar currentState
// state uvek treba da se setuje kroz setState , nikako direktno

// concat umesto push se koristi zato sto ne treba da se modifikuje originalni niz , nego da se pravi novi

// ovo setuje defaultnu vrednost koja ce se koristiti ako se properti ne prosledi
App.defaultProps = {
txt:"default text"
}

// kada imamo komponentu koja zahteva state-ove pravimo je kao class i ppristupamo propertijima this.props , a kada komponenta ne zahteva state-ove ona moze da bude obicna funkcija koja kao parametar dobija props object
// kada saljemo props parametre jedino string moze da se salje bez zagrada {}
// class komponenta mora da ima render metodu
// obicna komponenta samo vraca (div)

// u componentDidUpdate ne sme da stoji poziv ka necemu sto radi setState

// kada se radi return () zagrade moraju da budu odvojene od return
// ReactFragment tag menja div wrapper

myInput = React.createRef(); // koristi se da bi se uzela vrednost inputa forme
<input type="text" ref={this.myInput}>