

// LET , CONST

// glavna razlika izmedju var i let je u tome sto var moze da se deklarise vise puta sa istim imenom

var x = 5;
var x = 8;

let y = 6;
let y = 10; // ovo ce izbaciti gresku

// MAP I FOREACH

// razlika je u tome sto map pravi novi niz pa moze da se koristi za modifikovanje clanova , ali ne menja postojeci niz

//* SETOVANJE VISE PROMENJIVIH ISTOVREMENO

let x , y;
[x,y] = [100,200];
//x = 100 , y = 200

[x,y, ...rest] = [100,200,300,400,500];
// x = 100 , y = 200 , rest = [300,400,500]

const people = ['jhon' , 'beth' , 'mark'];
const [per1 , per2 , per3] = people;
// per1 = 'jhon' , per2 = 'beth' , per3 = 'mark'

const person = {
    name: 'John',
    age:32,
    city:'Miami',
    gender:'male'
}

const {name , age , city } = person;
// name = John , age = 32 , city = Miami

var voxel = {x: 3.6, y: 7.4, z: 6.54 };
const { x : a, y : b, z : c } = voxel // a = 3.6, b = 7.4, c = 6.54

// * SETS (unikatne vrednosti)

const set1 = new Set();

set1.add(100);
set1.add('string');
set1.add({name:'John'});
set1.add(true);

// jedan nacin dodavanja vrednosti
set1.add(100);
// poenta je u tome sto ne dozvoljava da se doda ista vrednost

// drugi nacin setovanja vrednosti
const set2 = new Set([1 , true , 'string']);

set1.has(100);
// proverava da li postoji vrednost 100

set1.has({name:'John'});//false
// ova provera moze samo za primitivne tipove

set1.delete(100);
// brise vrednost

for(let item of set1){
    console.log(item);
}
// prolazenje kroz vrednosti

// GETTERS AND SETTERS

class MyClass {
    constructor() {
    this.names_ = [];
    }
    set name(value) {
    this.names_.push(value);
    }
    get name() {
    return this.names_[this.names_.length - 1];
    }
}

const myClassInstance = new MyClass();
myClassInstance.name = 'Joe';
myClassInstance.name = 'Bob';
console.log(myClassInstance.name); // logs: "Bob"
console.log(myClassInstance.names_); // logs: ["Joe", "Bob"]

// ako se definise samo setter kada pokusamo da pristupimo propertiju sa myClassInstance.name vratice nam undefinied
// ako se definise samo getter kada pokusamo da pristupimo propertiju vratice nam vrednost koju smo definisali pri instanciranju get prop() {return 5;}
// tako da ne vredi ako posle toga napisemo instanca.prop = 10 , uvek ce vracati 5

// SWITCH

// switch moze imati vise case-eva
var x = "c"
switch (x) {
    case "a":
    case "b":
    case "c":
        console.log("Either a, b, or c was selected.");
        break;
    case "d":
        console.log("Only d was selected.");
        break;
    default:
        console.log("No case was matched.");
        break;
}

// LOOPS

// CONTINUE in LOOP

// u ovo slucaju nece logovati 1
for (var i = 0; i < 3; i++) {
    if (i === 1) {
        continue;
    }
    console.log(i);
}

// BREAK

// ovo prekida petlju tako da ce logovati samo 0
for (var i = 0; i < 3; i++) {
    if (i === 1) {
        break;
    }
    console.log(i);
}

// for petlja moze da se imenuje i da se prekine
outerloop:
    for (var i = 0;i<3;i++){
        innerloup:
            for (var j = 0;j <3; j++){
                console.log(i);
                console.log(j);
                if (j == 1){
                    break outerloop;
                }
            }
    }

    Object.keys(obj) // daje nam imena svih propertija
